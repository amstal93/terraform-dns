

![logo][logo]





 
# terraform-dns 


 
Manages AWS Route53 zones for multiple environments. 


---







## Requirements

In order to run this project you will need: 

- [Ubuntu][ubuntu] - Ubuntu is a complete Linux operating system, freely available with both community and professional support.
- [Terraform][terraform] - Write, Plan, and Create Infrastructure as Code




## Usage

```
make init install
make terraform/plan STAGE=development
make terraform/apply STAGE=development
```
Outputs:
```
account_id = 096373988534
arn = arn:aws:iam::096373988534:user/vhenrique
record_name = dev.skalesys.com
record_records = [
    ns-0.awsdns-00.com.,
    ns-512.awsdns-00.net.,
    ns-1536.awsdns-00.co.uk.,
    ns-1024.awsdns-00.org.
]
record_ttl = 30
record_type = NS
record_zone_id = Z2WMS2WM3AMHF7
recourd_fqdn = dev.skalesys.com
recourd_name = dev.skalesys.com
region = ap-southeast-2
user_id = AIDAIOLCLPNKFG2456DOW
zone_comment = [
    Managed by Terraform
]
zone_force_destroy = [
    false
]
zone_name = [
    dev.skalesys.com.
]
zone_tags = [
    {
        Environment = development,
        Name = sk-dev-webapp-dns,
        Namespace = sk,
        Office = Perth,
        Owner = SkaleSys,
        Stage = dev
    }
]
zone_vpc = [
    [
        map[vpc_region:ap-southeast-2 vpc_id:vpc-048f171535a31f361]
    ]
]
```

To destroy all the resources:
```
make terraform/destroy STAGE=development

```







## Makefile targets

```Available targets:

  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  docker/install                     	Install docker
  gomplate/install                   	Install gomplate
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  install                            	Install project requirements
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init-backend             	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/init                     	Initialize a Terraform working directory
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```








## References

For additional context, refer to some of these links. 

- [AWS Route53](https://aws.amazon.com/route53/) - A reliable and cost-effective way to route end users to Internet applications




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/Y20JJ_ddy9M) - Photo by israel palacio on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/




[aws]: https://aws.amazon.com/






[branches]: https://gitlab.com/skalesys/terraform-dns/branches
[commits]: https://gitlab.com/skalesys/terraform-dns/commits
[tags]: https://gitlab.com/skalesys/terraform-dns/tags
[contributors]: https://gitlab.com/skalesys/terraform-dns/graphs
[graph]: https://gitlab.com/skalesys/terraform-dns/network
[charts]: https://gitlab.com/skalesys/terraform-dns/charts


